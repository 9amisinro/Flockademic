// react-ga attempts to modify the DOM to add a <script> tag, so skip that
jest.mock('react-ga');

import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

import { ReactGAListener } from '../../src/components/reactGAListener/component';

it('should report the initial location to GA when initialising', () => {
  const mockReactGa = require('react-ga');
  const mockHistory = generateMockHistoryAtPathname('/arbitrary-location');

  mount(<ReactGAListener/>, generateRouterContext(mockHistory));

  expect(mockReactGa.pageview).toHaveBeenCalledTimes(1);
  expect(mockReactGa.pageview).toHaveBeenCalledWith('/arbitrary-location');
});

it('should report the changed location to GA when changing routes', () => {
  let listener;

  const mockReactGa = require('react-ga');
  const mockHistory = generateMockHistoryWithCallbackHandler((callback) => listener = callback);

  mount(<ReactGAListener/>, generateRouterContext(mockHistory));

  expect(mockReactGa.pageview).toHaveBeenCalledTimes(1);

  listener({ pathname: '/arbitrary-changed-location' });

  expect(mockReactGa.pageview).toHaveBeenCalledTimes(2);
  expect(mockReactGa.pageview).toHaveBeenCalledWith('/arbitrary-changed-location');
});


/* Helper functions */
function generateRouterContext(mockedHistory) {
  return {
    context: {
      router: {
        history: mockedHistory,
      },
    },
  };
}

function generateMockHistoryAtPathname(pathname: string) {
  return {
    location: {
      pathname: pathname,
    },
    listen: jest.fn(),
  };
}

function generateMockHistoryWithCallbackHandler(callbackHandler: Function) {
  return {
    location: {},
    listen: callbackHandler,
  };
}
