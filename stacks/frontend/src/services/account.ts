import * as localforage from 'localforage';

import { Account } from '../../../../lib/interfaces/Account';
import {
  GetAccountIdResponse,
  GetProfileResponse,
  PostNewSessionResponse,
  PostRefreshJwtRequest,
  PostRefreshJwtResponse,
  PostVerifyOrcidRequest,
  PostVerifyOrcidResponse,
} from '../../../../lib/interfaces/endpoints/accounts';
import { Session } from '../../../../lib/interfaces/Session';
import { fetchResource } from './fetchResource';

localforage.config({
  name: 'Flockademic',
});

function generateSession() {
  return fetchResource('/accounts', { method: 'POST' })
    .then((response) => {
      if (!response.ok) {
        throw new Error('Error fetching session details');
      }

      return response.json();
    })
    .then((data: PostNewSessionResponse) => {
      return Promise.all([
        localforage.setItem('session', data.session),
        localforage.setItem('session_jwt', data.jwt),
        localforage.setItem('session_refreshToken', data.refreshToken),
      ]).then(((storedData) => storedData[0]));
    });
}

export async function getSession(): Promise<Session> {
  const [ session, account ] = await Promise.all([
    localforage.getItem<Session>('session'),
    localforage.getItem<Account>('account'),
  ]);

  if (session === null) {
    return generateSession();
  }

  if (account !== null) {
    session.account = account;
  }

  return session;
}

export async function getToken(sessionId: string): Promise<string> {
  const jwt: string | null = await localforage.getItem<string>('session_jwt');

  if (jwt !== null) {
    const payload: { exp: number; iat: number; } = JSON.parse(atob(jwt.split('.')[1]));

    if (payload.exp > Date.now() / 1000) {
      return jwt;
    }
  }

  const refreshToken: string | null = await localforage.getItem<string>('session_refreshToken');

  if (refreshToken === null) {
    throw new Error('You have been logged out in another window, please log in and try again.');
  }

  const request: PostRefreshJwtRequest = { refreshToken };

  const response = await fetchResource(
    `/accounts/${sessionId}/jwt`,
    { method: 'POST', body: JSON.stringify(request)  },
  );

  const freshJwt: PostRefreshJwtResponse = await response.json();

  return localforage.setItem('session_jwt', freshJwt.jwt);
}

export async function getAccount(): Promise<Account & { orcid: string; } | null> {
  return localforage.getItem<Account & { orcid: string; }>('account');
}

export async function verifyOrcid(
  session: Session,
  token: string,
  code: string,
  redirectUri: string,
 ): Promise<Account & { orcid: string; }> {
  const requestBody: PostVerifyOrcidRequest = { jwt: token, redirectUri };
  const response = await fetchResource(
    `/accounts/${session.identifier}/orcid/verify/${code}`,
    { method: 'POST', body: JSON.stringify(requestBody) },
  );

  if (!response.ok) {
    return Promise.reject(new Error('There was an error verifying your ORCID.'));
  }

  const { jwt, account } = await response.json() as PostVerifyOrcidResponse;

  await localforage.setItem('session_jwt', jwt);
  await localforage.setItem('account', account);

  return account;
}

export async function getAccountId(orcid: string) {
 const response = await fetchResource(`/accounts/ids/${orcid}`);

 const data: GetAccountIdResponse = await response.json();

 return data.identifier;
}

export async function getProfiles(accountIds: string[]) {
 const response = await fetchResource(`/accounts/profiles/${accountIds.join('+')}`);

 const data: GetProfileResponse = await response.json();

 return data.profiles;
}
